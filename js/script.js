let users = []

function getUsers() {
    return fetch('https://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then(resUsers => resUsers)
}

function renderUsers(users) {
    const ul = document.querySelector('.list')
    ul.innerHTML = ''

    for (const user of users) {
        const li = document.createElement('li')
        li.classList.add('list-group-item')
        li.innerText = user.name + ' ' + user.email + ' ' + user.phone + ' ' + user.website
        ul.append(li)
    }
}


getUsers()
    .then(u => {
        users = u
        renderUsers(u)
    })

const input = document.querySelector('#exampleInputEmail1')

input.addEventListener('input', function (event) {
    const filterText = event.target.value

    if (filterText.length < 3) return

    renderUsers(users.filter(u =>
    {return u.username.toLowerCase().includes(filterText) ||
        u.email.toLowerCase().includes(filterText)
    }))

})
